# php-pm/[php-pm](https://packagist.org/packages/php-pm/php-pm)

PPM - PHP Process Manager

[![PHPPackages Rank](http://phppackages.org/p/php-pm/php-pm/badge/rank.svg)
](http://phppackages.org/p/php-pm/php-pm)
[![PHPPackages Referenced By](http://phppackages.org/p/php-pm/php-pm/badge/referenced-by.svg)
](http://phppackages.org/p/php-pm/php-pm)

## Unofficial documentation
* [*Symfony and ReactPHP Series — Chapter 2: PHP-PM to the rescue*
  ](https://medium.com/@apisearch/symfony-and-reactphp-series-chapter-2-e0a2246901e4)
  2019 Apisearch